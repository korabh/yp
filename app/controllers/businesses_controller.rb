class BusinessesController < ApplicationController
  before_action :authenticate_user!, except: :index

  def index
    @businesses = Business.all
  end

  def show
    @business = find_business
  end

  def new
    @business = Business.new
  end

  def create
    @business = build_business

    if @business.save
      flash[:notice] = 'Business was successfully created.'
      redirect_to @business
    else
      render :new
    end
  end

  private

    def build_business
      Business.new(business_params)
    end

    def find_business
      Business.find(params[:id])
    end

    def business_params
      params.require(:business).permit(
        :name,
        :description,
        :image,
        :additional_text,
        :email,
        :phone,
        :url,
        :services,
        :slogan
      )
  end
end
