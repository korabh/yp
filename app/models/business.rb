class Business < ActiveRecord::Base
  has_attached_file :image

  validates_attachment :image, presence: true
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  validates :name,
            :description,
            :email,
            :phone, presence: true
end
