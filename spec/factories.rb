FactoryGirl.define do
  factory :business do
    name "MyString"
    description "MyText"
    category ""
    city "MyString"
  end
  factory :user do
    email "test@example.com"
    password "password"

    factory :admin do
      admin true
    end
  end
end
