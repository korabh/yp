class CreateBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses do |t|
      t.string :name, null: false
      t.string :description
      t.text :additional_text
      t.string :email
      t.string :phone
      t.attachment :image
      t.string :url
      t.string :services
      t.string :slogan

      t.timestamps null: false
    end
  end
end
