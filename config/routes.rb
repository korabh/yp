Rails.application.routes.draw do
  devise_for :users

  resources :businesses

  mount RailsAdmin::Engine => "/admin", as: "rails_admin"
end
